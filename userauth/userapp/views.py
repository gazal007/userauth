from django.shortcuts import render
from rest_framework import status
from userapp.models import User
from rest_framework import viewsets, serializers


from rest_framework.response import Response
from rest_framework.decorators import api_view

from userapp.serializer import RegisterSerializer, UserDetailsSerializer
from rest_framework.authtoken.models import Token


# Create your views here.
@api_view(['POST',])
def registration_view(request):

    if request.method == 'POST':
        serializer = RegisterSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            data['response'] = "successfully registered a new user."
            data['email'] = account.email
            data['username'] = account.username
            token = Token.objects.get(user=account).key
            data['token'] = token
        else:
            data = serializer.ValueError
        return Response(data)

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserDetailsSerializer