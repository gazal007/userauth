from rest_framework import routers
from userapp.views import UserViewSet




router = routers.DefaultRouter()
router.register(r'users', UserViewSet)